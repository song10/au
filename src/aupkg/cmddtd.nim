import std/[pegs, strutils, strformat]
import cmd

template m(x: int): string = matches[x]

proc dtd*(info = "", pths: seq[string]): Exitcode =
  result = ecNG
  while pths.len > 0: # once
    let
      fn = pths[0]
      all = readFile(fn)
      lines = all.splitLines
    for ln in lines:
      if ln =~ peg"\s* {[0-9a-f]+} ':' \s+ {[0-9a-f]+} \s+ {[0-9a-z.]+} \s+ {\S+} (\s+ {'#' \s+ [0-9a-f]+} \s+)?":
        var opn = m(3)
        if '(' in opn:
          opn = opn.multiReplace(("(", "\\("), (")", "\\)"))
        if m(4) == "":
          echo &"[ 	]+{m(0)}:[ 	]+{m(1)}[ 	]+{m(2)}[ 	]+{opn}"
        else:
          echo &"[ 	]+{m(0)}:[ 	]+{m(1)}[ 	]+{m(2)}[ 	]+{opn} {m(4)} .*"
      elif ln =~ peg"@ ':' @ 'file format'":
        echo ".*:[ 	]+file format .*"
      elif ln =~ peg"[0-9a-f]+ \s+ '<' @@ '>'":
        echo &"0+000 <{m(0)}>:"
      else:
        echo ln
    result = ecOK
    break # once
