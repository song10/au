import std/[sequtils, strutils, strformat]
import cmd, modgas

proc dump_side_by_side*(ax, bx: GasOpc) =
  # k,v: mne, "line[[\nline]]*"
  let
    (aks, bks) = (ax.keys.toSeq, bx.keys.toSeq)
    both = aks.filterIt(it in bks)
    aksex = aks.filterIt(it notin both)
    bksex = bks.filterIt(it notin both)
  echo "=== both ==="
  for k in both:
    echo &"""-
  - {ax[k]}
  - {bx[k]}"""
  echo "=== A's ==="
  for k in aksex: echo ax[k]
  echo "=== B's ==="
  for k in bksex: echo bx[k]

proc cmpgas*(isa: string, pths: seq[string]): Exitcode =
  # isa: isa[[:isa]]*
  # pths: [ "path[[:old]]", ...]
  result = ecNG
  while pths.len > 1: # once
    var opcs: seq[GasOpc]
    let isas = isa.split ':'
    for x in [pths[0], pths[1]]:
      var ws = x.split ':'
      if ws.len == 1: ws.add "new"
      opcs.add gas_read_opc(ws[0], ws[1], isas)
    dump_side_by_side(opcs[0], opcs[1])
    result = ecOK
    break # once
