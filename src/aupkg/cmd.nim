type
  Exitcode* = enum
    ecOK = 0, ecNG = 1, ecFATAL = 2
