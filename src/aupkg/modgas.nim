import std/[sequtils, strutils, pegs, tables]

export tables

type
  GasOpc* = OrderedTable[string, string]

let
  #  name, xlen, isa, operands, match, mask, match_func, pinfo.
  # {"unimp", 0, INSN_CLASS_C, "",  0, (int) 0xffffU, match_opcode, INSN_ALIAS },
  opc_pattern = peg"""\skip(\s*)
    s <- '{' struct '}' ','?
    struct <- mnemonic ',' {xlen} ',' {isa} ',' operands ',' {match} ',' {mask} ',' {func} ',' {info}
    mnemonic <- '"' @@ '"'
    xlen <- \d+
    isa <- \ident
    operands <- '"' @@ '"'
    match <- symval ('|' symval)*
    mask <- symval ('|' symval)*
    func <- \ident
    info <- symval ('|' symval)*
    symval <- ('(' @ ')' \s*)? \ident / value
    value <- \d+ / (i'0x' [a-fA-F0-9]+)
    """
  #  name, xlen, isa, operands, match, mask, match_func, pinfo.
  # {"unimp", 0, {"C", 0},     "",  0, (int) 0xffffU, match_opcode, INSN_ALIAS },
  opc_pattern_old = peg"""\skip(\s*)
    s <- '{' struct '}' ','?
    struct <- mnemonic ',' {xlen} ',' isa ',' operands ',' {match} ',' {mask} ',' {func} ',' {info}
    mnemonic <- '"' @@ '"'
    xlen <- \d+
    isa <- '{' @@ '}'
    operands <- '"' @@ '"'
    match <- symval ('|' symval)*
    mask <- symval ('|' symval)*
    func <- \ident
    info <- symval ('|' symval)*
    symval <- ('(' @ ')' \s*)? \ident / value
    value <- \d+ / (i'0x' [a-fA-F0-9]+)
    """

proc gas_read_opc_act(fn: string, isas: seq[string]): GasOpc =
  let all = readFile(fn)
  let lines = all.splitLines
  for ln in lines:
    if ln =~ opc_pattern:
      let (mne, isa) = (matches[0], matches[2])
      let is_isa = isas.anyIt(it in isa)
      if is_isa:
        if mne in result:
          result[mne] &= "\n" & ln
        else:
          result[mne] = ln

proc gas_read_opc_act_old(fn: string, isas: seq[string]): GasOpc =
  let all = readFile(fn)
  let lines = all.splitLines
  for ln in lines:
    if ln =~ opc_pattern_old:
      let (mne, isa) = (matches[0], matches[2])
      let is_isa = isas.anyIt(it in isa)
      if is_isa:
        if mne in result:
          result[mne] &= "\n" & ln
        else:
          result[mne] = ln

proc gas_read_opc*(fn, fmt: string, isas: seq[string]): GasOpc =
  case fmt:
  of "old": result = gas_read_opc_act_old(fn, isas)
  else: result = gas_read_opc_act(fn, isas)
