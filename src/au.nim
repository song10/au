import aupkg/[cmdcmpgas, cmddtd]

when isMainModule:
  import cligen
  dispatchMulti([cmpgas], [dtd, help = {"info": "convert .dis to .d"}])
