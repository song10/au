# Package

version       = "0.1.0"
author        = "Rex Zhuo"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["au"]


# Dependencies

requires "nim >= 1.6.4"
requires "cligen"
